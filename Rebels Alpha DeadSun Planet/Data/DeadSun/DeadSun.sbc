<?xml version="1.0" encoding="utf-8"?>

<Definitions xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <PlanetGeneratorDefinitions>
    <PlanetGeneratorDefinition>
      <Id>
        <TypeId>PlanetGeneratorDefinition</TypeId>
        <SubtypeId>DeadSun</SubtypeId>
      </Id>
      <PlanetMaps Material="true" Ores="true" Biome="false" Occlusion="true" />

      <SurfaceDetail>
        <Texture>Data\PlanetDataFiles\DeadSun</Texture>
        <Size>384</Size>
        <Scale>5</Scale>
        <Slope Min="20" Max="90" />
        <Transition>7.5</Transition>
      </SurfaceDetail>

      <DistortionTable>
        <Distortion Type="Perlin" Value="92" Frequency="10" Height="4" LayerCount="6" />
      </DistortionTable>

      <OreMappings>
        <Ore Value="14" Type="Flerow" Start="80" Depth="8" />
        <Ore Value="4" Type="Flerow" Start="96" Depth="11" />

        <Ore Value="16" Type="Flerow" Start="70" Depth="1" />
        <Ore Value="6" Type="Flerow" Start="45" Depth="3" />
        
        <Ore Value="6" Type="Lorens" Start="24" Depth="5" />
        <Ore Value="14" Type="Lorens" Start="30" Depth="8" />

        <Ore Value="8" Type="Lorens" Start="30" Depth="4" />
        <Ore Value="11" Type="Lorens" Start="50" Depth="6" />
        
        <Ore Value="12" Type="Nihonium" Start="85" Depth="5" />
        <Ore Value="5" Type="Nihonium" Start="31" Depth="8" />

        <Ore Value="8" Type="Nihonium" Start="51" Depth="4" />
        <Ore Value="2" Type="Nihonium" Start="11" Depth="6" />
        
        <Ore Value="10" Type="Tau" Start="60" Depth="10" />
        <Ore Value="3" Type="Tau" Start="30" Depth="7" />

        <Ore Value="2" Type="Tau" Start="10" Depth="3" />
        <Ore Value="8" Type="Tau" Start="44" Depth="8" />
      </OreMappings>

      <SoundRules>
        <SoundRule>
          <Height Min="0" Max="1.2" />
          <Latitude Min="0" Max="90" />
          <SunAngleFromZenith Min="0" Max="180" />
          <EnvironmentSound>AmbMoonDayLoop</EnvironmentSound>
        </SoundRule>
      </SoundRules>

      <ComplexMaterials>
        <MaterialGroup Name="LO" Value="255">
          <Rule>
            <Layers>
              <Layer Material="RykeLava" Depth="25" />
            </Layers>
            <Height Min="-0.001" Max="0.001" />
            <Latitude Min="0" Max="90" />
            <Slope Min="0" Max="3" />
          </Rule>
        </MaterialGroup>
        <MaterialGroup Name="AO" Value="0">
          <Rule>
            <Layers>
              <Layer Material="RykeLava1" Depth="25" />
            </Layers>
            <Height Min="0" Max="0.008" />
            <Latitude Min="0" Max="90" />
            <Slope Min="0" Max="3" />
          </Rule>
          <Rule>
            <Layers>
              <Layer Material="RykeLava1" Depth="25" />
            </Layers>
            <Height Min="0" Max="1" />
            <Latitude Min="0" Max="90" />
            <Slope Min="4" Max="5" />
          </Rule>
          <Rule>
            <Layers>
              <Layer Material="RykeLava" Depth="25" />
            </Layers>
            <Height Min="0" Max="1" />
            <Latitude Min="0" Max="90" />
            <Slope Min="5" Max="15" />
          </Rule>
          <Rule>
            <Layers>
              <Layer Material="RykeLava1" Depth="25" />
            </Layers>
            <Height Min="0" Max="1" />
            <Latitude Min="0" Max="90" />
            <Slope Min="15" Max="30" />
          </Rule>
          <Rule>
            <Layers>
              <Layer Material="RykeLava1" Depth="25" />
            </Layers>
            <Height Min="0" Max="1" />
            <Latitude Min="0" Max="90" />
            <Slope Min="30" Max="90" />
          </Rule>
        </MaterialGroup>
      </ComplexMaterials>
      <EnvironmentItems>
        <Item>
          <Biomes>
            <Biome>0</Biome>
          </Biomes>
          <Materials>
            <Material>RykeSoil</Material>
          </Materials>
          <Items>
            <Item TypeId="MyObjectBuilder_VoxelMapStorageDefinition" GroupId="Stones" ModifierId="RykeRock2"
                  Density="0.007" />
          </Items>
          <Rule>
            <Height Min="0" Max="1" />
            <Latitude Min="0" Max="90" />
            <Slope Min="5" Max="15" />
          </Rule>
        </Item>
      </EnvironmentItems>

      <DefaultSurfaceMaterial Material="RykeLava" MaxDepth="25" />
      <DefaultSubSurfaceMaterial Material="RykeLava" />

      <MaximumOxygen>0.1</MaximumOxygen>
      <HasAtmosphere>true</HasAtmosphere>

      <GravityFalloffPower>3</GravityFalloffPower>


      <SurfaceGravity>2.3</SurfaceGravity>

      <Deviation Min="0.005" Max="0.003" />
      <MaterialsMaxDepth Min="4000" Max="4000" />
      <MaterialsMinDepth Min="20" Max="20" />
<AtmosphereSettings>
      <RayleighScattering>
        <X>255</X>
        <Y>255</Y>
        <Z>255</Z>
      </RayleighScattering>
      <MieScattering>40</MieScattering>
      <MieColorScattering>
        <X>200</X>
        <Y>200</Y>
        <Z>200</Z>
      </MieColorScattering>
      <RayleighHeight>30</RayleighHeight>
      <RayleighHeightSpace>10</RayleighHeightSpace>
      <RayleighTransitionModifier>2.9</RayleighTransitionModifier>
      <MieHeight>30</MieHeight>
      <MieG>0.998</MieG>
      <Intensity>17</Intensity>
      <SeaLevelModifier>0.092</SeaLevelModifier>
      <AtmosphereTopModifier>1</AtmosphereTopModifier>
      <FogIntensity>40</FogIntensity>
      <Scale>10</Scale>
    </AtmosphereSettings>

      <Atmosphere>
        <Breathable>true</Breathable>
        <OxygenDensity>0</OxygenDensity>
        <Density>1.2</Density>
        <LimitAltitude>8.0</LimitAltitude>
      </Atmosphere>

      <HillParams Min="-0.02" Max="0.06" />


      <CloudLayers>
        <CloudLayer>
          <Model>Models/Environment/Sky/CloudSphere.mwm</Model>
          <Textures>
            <Texture>Textures/Models/Environment/Sky/Fog_vsha.dds</Texture>
          </Textures>
          <RelativeAltitude>0.1</RelativeAltitude>
          <RotationAxis>
            <X>0</X>
            <Y>0</Y>
            <Z>0</Z>
          </RotationAxis>
          <AngularVelocity>0.0001</AngularVelocity>
          <InitialRotation>0</InitialRotation>
          <ScalingEnabled>true</ScalingEnabled>
          <FadeOutRelativeAltitudeStart>0.1</FadeOutRelativeAltitudeStart>
          <FadeOutRelativeAltitudeEnd>0.4</FadeOutRelativeAltitudeEnd>
          <ApplyFogRelativeDistance>0</ApplyFogRelativeDistance>
        </CloudLayer>

        <CloudLayer>
          <!-- Far layer -->
          <Model>Models/Environment/Sky/CloudSphere.mwm</Model>
          <Textures>
            <Texture>Textures/Models/Environment/Sky/Landsky_texture.dds</Texture>
          </Textures>
          <RelativeAltitude>0.3</RelativeAltitude>
          <RotationAxis>
            <X>0</X>
            <Y>0</Y>
            <Z>0</Z>
          </RotationAxis>
          <AngularVelocity>0.0002</AngularVelocity>
          <InitialRotation>3.2</InitialRotation>
          <ScalingEnabled>true</ScalingEnabled>
          <FadeOutRelativeAltitudeStart>1</FadeOutRelativeAltitudeStart>
          <FadeOutRelativeAltitudeEnd>0.6</FadeOutRelativeAltitudeEnd>
          <ApplyFogRelativeDistance>0</ApplyFogRelativeDistance>
        </CloudLayer>

        <!-- Near layers -->
        <CloudLayer>
          <Model>Models/Environment/Sky/CloudSphere.mwm</Model>
          <Textures>
            <Texture>Textures/Models/Environment/Sky/Landsky_texture.dds</Texture>
          </Textures>
          <RelativeAltitude>0.1</RelativeAltitude>
          <RotationAxis>
            <X>0</X>
            <Y>0</Y>
            <Z>0</Z>
          </RotationAxis>
          <AngularVelocity>0.0006</AngularVelocity>
          <InitialRotation>0.95</InitialRotation>
          <ScalingEnabled>true</ScalingEnabled>
          <FadeOutRelativeAltitudeStart>1.1</FadeOutRelativeAltitudeStart>
          <FadeOutRelativeAltitudeEnd>0.7</FadeOutRelativeAltitudeEnd>
          <ApplyFogRelativeDistance>0</ApplyFogRelativeDistance>
        </CloudLayer>
        <CloudLayer>
          <Model>Models/Environment/Sky/CloudSphere.mwm</Model>
          <Textures>
            <Texture>Textures/Models/Environment/Sky/Landsky_texture.dds</Texture>
          </Textures>
          <RelativeAltitude>0.2</RelativeAltitude>
          <RotationAxis>
            <X>0</X>
            <Y>0</Y>
            <Z>0</Z>
          </RotationAxis>
          <AngularVelocity>0</AngularVelocity>
          <InitialRotation>0</InitialRotation>
          <ScalingEnabled>true</ScalingEnabled>
          <FadeOutRelativeAltitudeStart>1.1</FadeOutRelativeAltitudeStart>
          <FadeOutRelativeAltitudeEnd>0.7</FadeOutRelativeAltitudeEnd>
          <ApplyFogRelativeDistance>0</ApplyFogRelativeDistance>
        </CloudLayer>

        <CloudLayer>
          <Model>Models/Environment/Sky/CloudSphere.mwm</Model>
          <Textures>
            <Texture>Textures/Models/Environment/Sky/Landsky_texture.dds</Texture>
          </Textures>
          <RelativeAltitude>0.6</RelativeAltitude>
          <RotationAxis>
            <X>0</X>
            <Y>0</Y>
            <Z>0</Z>
          </RotationAxis>
          <AngularVelocity>0</AngularVelocity>
          <InitialRotation>0</InitialRotation>
          <ScalingEnabled>true</ScalingEnabled>
          <FadeOutRelativeAltitudeStart>0.1</FadeOutRelativeAltitudeStart>
          <FadeOutRelativeAltitudeEnd>0.5</FadeOutRelativeAltitudeEnd>
          <ApplyFogRelativeDistance>0</ApplyFogRelativeDistance>
        </CloudLayer>

        <CloudLayer>
          <Model>Models/Environment/Sky/CloudSphere.mwm</Model>
          <Textures>
            <Texture>Textures/Models/Environment/Sky/Fog_vsha.dds</Texture>
          </Textures>
          <RelativeAltitude>1.0</RelativeAltitude>
          <RotationAxis>
            <X>0</X>
            <Y>0</Y>
            <Z>0</Z>
          </RotationAxis>
          <AngularVelocity>0</AngularVelocity>
          <InitialRotation>0</InitialRotation>
          <ScalingEnabled>true</ScalingEnabled>
          <FadeOutRelativeAltitudeStart>0.8</FadeOutRelativeAltitudeStart>
          <FadeOutRelativeAltitudeEnd>1.5</FadeOutRelativeAltitudeEnd>
          <ApplyFogRelativeDistance>0</ApplyFogRelativeDistance>
        </CloudLayer>

        <CloudLayer>
          <Model>Models/Environment/Sky/CloudSphere.mwm</Model>
          <Textures>
            <Texture>Textures/Models/Environment/Sky/Fog_vsha.dds</Texture>
          </Textures>
          <RelativeAltitude>3.0</RelativeAltitude>
          <RotationAxis>
            <X>0</X>
            <Y>0</Y>
            <Z>0</Z>
          </RotationAxis>
          <AngularVelocity>0</AngularVelocity>
          <InitialRotation>0</InitialRotation>
          <ScalingEnabled>true</ScalingEnabled>
          <FadeOutRelativeAltitudeStart>0.8</FadeOutRelativeAltitudeStart>
          <FadeOutRelativeAltitudeEnd>0.9</FadeOutRelativeAltitudeEnd>
          <ApplyFogRelativeDistance>0</ApplyFogRelativeDistance>
        </CloudLayer>


        <CloudLayer>
          <Model>Models/Environment/Sky/CloudSphere.mwm</Model>
          <Textures>
            <Texture>Textures/Models/Environment/Sky/Fog_vsha.dds</Texture>
          </Textures>
          <RelativeAltitude>4.0</RelativeAltitude>
          <RotationAxis>
            <X>0</X>
            <Y>0</Y>
            <Z>0</Z>
          </RotationAxis>
          <AngularVelocity>0</AngularVelocity>
          <InitialRotation>0</InitialRotation>
          <ScalingEnabled>true</ScalingEnabled>
          <FadeOutRelativeAltitudeStart>0.1</FadeOutRelativeAltitudeStart>
          <FadeOutRelativeAltitudeEnd>0.5</FadeOutRelativeAltitudeEnd>
          <ApplyFogRelativeDistance>0</ApplyFogRelativeDistance>
        </CloudLayer>

        <CloudLayer>
          <Model>Models/Environment/Sky/CloudSphere.mwm</Model>
          <Textures>
            <Texture>Textures/Models/Environment/Sky/Fog_vsha.dds</Texture>
          </Textures>
          <RelativeAltitude>5.0</RelativeAltitude>
          <RotationAxis>
            <X>0</X>
            <Y>0</Y>
            <Z>0</Z>
          </RotationAxis>
          <AngularVelocity>0</AngularVelocity>
          <InitialRotation>0</InitialRotation>
          <ScalingEnabled>true</ScalingEnabled>
          <FadeOutRelativeAltitudeStart>0.1</FadeOutRelativeAltitudeStart>
          <FadeOutRelativeAltitudeEnd>0.1</FadeOutRelativeAltitudeEnd>
          <ApplyFogRelativeDistance>0</ApplyFogRelativeDistance>
        </CloudLayer>

      </CloudLayers>

    </PlanetGeneratorDefinition>

  </PlanetGeneratorDefinitions>
</Definitions>