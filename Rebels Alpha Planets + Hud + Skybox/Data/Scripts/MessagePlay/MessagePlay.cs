/*
 *  Copyright (C) Chris Courson, 2016. All rights reserved.
 * 
 *  This file is part of MessagePlay, a Space Engineers mod available through Steam.
 *
 *  MessagePlay is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MessagePlay is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MessagePlay.  If not, see <http://www.gnu.org/licenses/>.
 */
using Sandbox.ModAPI;
using System;
using System.IO;
using VRage.Game.Components;

namespace MessagePlay
{
	[MySessionComponentDescriptor(MyUpdateOrder.BeforeSimulation)]
    public class MessagePlay : MySessionComponentBase
    {
        private bool isInit;
        private HelpTopics helpTopics;
        private WelcomePanel welcomePanel;
        private Announcements announcements;
        private bool welcomePanelShown;

        public void Init()
        {
            isInit = true;

			MyAPIGateway.Multiplayer.RegisterMessageHandler(1001, MessageHandler);

			if (MyAPIGateway.Multiplayer.IsServer) LoadConfigurations();

            if (!MyAPIGateway.Multiplayer.IsServer || MyAPIGateway.Session.Player != null)
            {
                MyAPIGateway.Utilities.MessageEntered += MessageEnteredHandler;
                Message.RequestWelcome();
                Message.RequestHelp();
            }
        }

        private void LoadConfigurations()
        {
			if (announcements != null) announcements.CloseTimer();

			helpTopics = Configuration<HelpTopics>.LoadFromLocalStorage();
			if (helpTopics == null)
			{
				helpTopics = new HelpTopics()
				{
					enabled = false,
				};
				helpTopics.topics.Add(new HelpTopic()
				{
					key = "key",
					title = "Title",
					subtitle = "Subtitle",
					description = "Quick command\r\n"
				});
					
				Configuration<HelpTopics>.CreateInLocalStorage(helpTopics);
			}

			welcomePanel = Configuration<WelcomePanel>.LoadFromLocalStorage();
			if (welcomePanel == null)
			{
				welcomePanel = new WelcomePanel()
				{
					enabled = true,
					title = "WARNING",
					subtitle = "Permanent Death",
					description = "If U chose Starting ship (New Game) or Space Suit U will lose all yours grids ownership and GPS locations, and start as New Player whit new ID.\r\n\r\nU can grind down your old grids or hack them if U gonna find them on time because those grids whit old ID will be erased after 14 days like any others non active Players.\r\n\r\nSo be aware!\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\nRead the description and server rules.\r\nChat command: /motd",
					buttonCaption = "I understand"
				};

				Configuration<WelcomePanel>.CreateInLocalStorage(welcomePanel);
			}

			announcements = Configuration<Announcements>.LoadFromLocalStorage();
			if (announcements == null)
			{
				announcements = new Announcements()
				{
					intervalMinutes = 5,
                    enabled = true,
				};
				announcements.announcements.Add(new Announcement()
				{
					sender = "Info",
					message = "Want to know about everything that happens on the servers?\r\nJoin the Discord!\r\nhttps://discord.gg/KWmrpey"
				});
                announcements.announcements.Add(new Announcement()
                {
                    sender = "Info",
                    message = "Server description (Command: /motd)"
                });
                announcements.announcements.Add(new Announcement()
                {
                    sender = "Info",
                    message = "We invite you to  www.rebels-games.com"
                });
                announcements.announcements.Add(new Announcement()
                {
                    sender = "Info",
                    message = "Donate with: paypal.me/lostspace"
                });
                announcements.announcements.Add(new Announcement()
                {
                    sender = "Info",
                    message = "Can buy virtual Credits and Super Credits (Command: /donate)"
                });
                announcements.announcements.Add(new Announcement()
                {
                    sender = "Info",
                    message = "Simple commands:\r\n/help - All commands\r\n/joinhelp - Server jump info\r\n/discord - Discord server link\r\n/oreinfo - Which ore are on planet and in space"
                });
                announcements.announcements.Add(new Announcement()
                {
                    sender = "Info",
                    message = "Simple commands:\r\n/motd - Info about servers\r\n!grids list - Show GPS coordinates of Your grids\r\n/donate - Can buy virtual Credits and Super Credits"
                });
                announcements.announcements.Add(new Announcement()
                {
                    sender = "Info",
                    message = "Monthly server maintenance cost:133 EURO\r\nHelp us :)"
                });
                announcements.announcements.Add(new Announcement()
                {
                    sender = "Info",
                    message = "On the first day of the new month asteroids will change position"
                });
                announcements.announcements.Add(new Announcement()
                {
                    sender = "Info",
                    message = "If you want your object not removed, do not forget to install the beacon.\r\nOnce a day there is a thorough cleaning of objects without beacon."
                });

				Configuration<Announcements>.CreateInLocalStorage(announcements);
			}

			announcements.InitTimer();
        }

        // this is called at fps rate (60Hz default)
        public override void UpdateBeforeSimulation()
        {
            base.UpdateBeforeSimulation();

            if (!isInit) Init();
        }

        private void MessageHandler(byte[] obj)
        {
            Message message = new Message(obj);

            switch (message.messageType)
            {
                case MessageType.WelcomeRequest:
                    if (MyAPIGateway.Multiplayer.IsServer)
                        new Message(MessageType.WelcomeResponse, Configuration<WelcomePanel>.ToXML(welcomePanel)).SendTo(message.playerId);
                    break;
                case MessageType.WelcomeResponse:
                    if (MyAPIGateway.Session.Player != null)
                    {
                        welcomePanel = Configuration<WelcomePanel>.FromXML(message.message);
                        if (welcomePanel.enabled && !welcomePanelShown) welcomePanel.Show();
                        welcomePanelShown = true;
                    }
                    break;
                case MessageType.HelpRequest:
                    if (MyAPIGateway.Multiplayer.IsServer)
                        new Message(MessageType.HelpResponse, Configuration<HelpTopics>.ToXML(helpTopics)).SendTo(message.playerId);
                    break;
                case MessageType.HelpResponse:
                    if (MyAPIGateway.Session.Player != null)
                        helpTopics = Configuration<HelpTopics>.FromXML(message.message);
                    break;
                case MessageType.Announcement:
                    Announcement announcement = Configuration<Announcement>.FromXML(message.message);
                    MyAPIGateway.Utilities.ShowMessage(announcement.sender, announcement.message);
                    break;
				case MessageType.ScheduledAnnouncement:
					ScheduledAnnouncement scheduledAnnouncement = Configuration<ScheduledAnnouncement>.FromXML(message.message);
					MyAPIGateway.Utilities.ShowMessage(scheduledAnnouncement.sender, scheduledAnnouncement.message);
					break;
				case MessageType.ReloadRequest:
                    if (MyAPIGateway.Multiplayer.IsServer)
                    {
                        LoadConfigurations();
                        new Message(MessageType.WelcomeResponse, Configuration<WelcomePanel>.ToXML(welcomePanel)).SendToOthers();
                        new Message(MessageType.HelpResponse, Configuration<HelpTopics>.ToXML(helpTopics)).SendToOthers();
                        new Message(MessageType.ReloadResponse, null).SendTo(message.playerId);
                    }
                    break;
                case MessageType.ReloadResponse:
                    MyAPIGateway.Utilities.ShowMessage("MessagePlay", "Reloaded.");
                    break;
            }
        }

        protected override void UnloadData()
        {
            MyAPIGateway.Multiplayer.UnregisterMessageHandler(1001, MessageHandler);
            if (MyAPIGateway.Multiplayer.IsServer)
            {
				announcements.CloseTimer();
            }
            if (!MyAPIGateway.Multiplayer.IsServer || MyAPIGateway.Session.Player != null)
            {
                MyAPIGateway.Utilities.MessageEntered -= MessageEnteredHandler;
            }
        }

        internal void MessageEnteredHandler(string messageText, ref bool sendToOthers)
        {

            if (messageText.Equals("mp reload") && MyAPIGateway.Session.Player.IsAdmin)
            {
                Message.RequestReload();
                sendToOthers = false;
            }
            else if (messageText.Equals("motd"))
            {
                welcomePanel.Show();
                sendToOthers = false;
            }
            else if (helpTopics.HasHelpKey(messageText))
            {
                helpTopics.ShowHelpFor(messageText);
                sendToOthers = false;
            }
        }

        public static string ExpandMacros(ref string text)
        {
            string expandedText = string.Copy(text);
            int i = 0;

            try
            {
                expandedText = expandedText.Replace("@PlayerID", MyAPIGateway.Multiplayer.MyId.ToString()); i++;
                expandedText = expandedText.Replace("@PlayerName", MyAPIGateway.Multiplayer.MyName); i++;
                expandedText = expandedText.Replace("@ServerID", MyAPIGateway.Multiplayer.ServerId.ToString()); i++;
                expandedText = expandedText.Replace("@AssemblerEfficiencyMultiplier", MyAPIGateway.Session.AssemblerEfficiencyMultiplier.ToString()); i++;
                expandedText = expandedText.Replace("@AssemblerSpeedMultiplier", MyAPIGateway.Session.AssemblerSpeedMultiplier.ToString()); i++;
                expandedText = expandedText.Replace("@AutoHealing", MyAPIGateway.Session.AutoHealing.ToString()); i++;
                expandedText = expandedText.Replace("@CargoShipsEnabled", MyAPIGateway.Session.CargoShipsEnabled.ToString()); i++;
                expandedText = expandedText.Replace("@CreativeMode", MyAPIGateway.Session.CreativeMode.ToString()); i++;
                expandedText = expandedText.Replace("@Description", MyAPIGateway.Session.Description); i++;
                expandedText = expandedText.Replace("@ElapsedPlayTime", MyAPIGateway.Session.ElapsedPlayTime.ToString()); i++;
                expandedText = expandedText.Replace("@EnvironmentHostility", MyAPIGateway.Session.EnvironmentHostility.ToString()); i++;
                expandedText = expandedText.Replace("@GameDateTime", MyAPIGateway.Session.GameDateTime.ToString()); i++;
                expandedText = expandedText.Replace("@GrinderSpeedMultiplier", MyAPIGateway.Session.GrinderSpeedMultiplier.ToString()); i++;
                expandedText = expandedText.Replace("@HackSpeedMultiplier", MyAPIGateway.Session.HackSpeedMultiplier.ToString()); i++;
                expandedText = expandedText.Replace("@InventoryMultiplier", MyAPIGateway.Session.InventoryMultiplier.ToString()); i++;
                expandedText = expandedText.Replace("@MaxFloatingObjects", MyAPIGateway.Session.MaxFloatingObjects.ToString()); i++;
                expandedText = expandedText.Replace("@MaxPlayers", MyAPIGateway.Session.MaxPlayers.ToString()); i++;
                expandedText = expandedText.Replace("@WorldName", MyAPIGateway.Session.Name.ToString()); i++;
                expandedText = expandedText.Replace("@IsDedicated", MyAPIGateway.Utilities.IsDedicated.ToString()); i++;
                expandedText = expandedText.Replace("@AutoSaveInMinutes", MyAPIGateway.Session.AutoSaveInMinutes.ToString()); i++;
                expandedText = expandedText.Replace("@IsAdmin", MyAPIGateway.Session.Player.IsAdmin.ToString()); i++;
                expandedText = expandedText.Replace("@IsPromoted", MyAPIGateway.Session.Player.IsPromoted.ToString()); i++;
                expandedText = expandedText.Replace("@SteamUserId", MyAPIGateway.Session.Player.SteamUserId.ToString()); i++;
                expandedText = expandedText.Replace("@DisplayName", MyAPIGateway.Session.Player.DisplayName.ToString()); i++;
                expandedText = expandedText.Replace("@RefinerySpeedMultiplier", MyAPIGateway.Session.RefinerySpeedMultiplier.ToString()); i++;
                expandedText = expandedText.Replace("@TimeOnBigShip", MyAPIGateway.Session.TimeOnBigShip.ToString()); i++;
                expandedText = expandedText.Replace("@TimeOnFoot", MyAPIGateway.Session.TimeOnFoot.ToString()); i++;
                expandedText = expandedText.Replace("@TimeOnJetpack", MyAPIGateway.Session.TimeOnJetpack.ToString()); i++;
                expandedText = expandedText.Replace("@TimeOnSmallShip", MyAPIGateway.Session.TimeOnSmallShip.ToString()); i++;
                expandedText = expandedText.Replace("@WeaponsEnabled", MyAPIGateway.Session.WeaponsEnabled.ToString()); i++;
                expandedText = expandedText.Replace("@WelderSpeedMultiplier", MyAPIGateway.Session.WelderSpeedMultiplier.ToString()); i++;
            }
            catch (Exception ex)
            {
                using (TextWriter writer = MyAPIGateway.Utilities.WriteFileInLocalStorage("errorlog.txt", typeof(MessagePlay)))
                {
                    writer.Write(ex.ToString() + "\r\n\r\n index: (" + i.ToString() + ")\r\n" + expandedText);
                }
            }

            return expandedText;
        }
    }
}
